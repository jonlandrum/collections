package com.jonlandrum.collections.BinarySearchTree.AVLTree;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedAVLTreeTest {
    @Test
    public void testEmptyConstructor() {
        LinkedAVLTree tree = new LinkedAVLTree<>();
        assertEquals(null, tree.getRoot());
    }

    @Test
    public void testNodeConstructor() {
        LinkedAVLTree tree = new LinkedAVLTree<>(new LinkedAVLNode<>("root"));
        assertEquals("root", tree.getRoot().toString());
    }

    @Test
    public void testGetRoot_RootIsNull() {
        LinkedAVLTree testGetRoot = new LinkedAVLTree<>();
        assertEquals(null, testGetRoot.getRoot());
    }

    @Test
    public void testGetRoot_RootIsNotNull() {
        LinkedAVLTree testGetRoot = new LinkedAVLTree<>(new LinkedAVLNode<>("test"));
        assertEquals("test", testGetRoot.getRoot().toString());
    }

    @Test
    public void testInsertIntoEmptyTree() {
        LinkedAVLTree<String> testInsert = new LinkedAVLTree<>();
        assertFalse(testInsert.hasRoot());
        testInsert.insert("1");
        assertTrue(testInsert.hasRoot());
    }

//    @Test
//    public void testInsertIntoNonEmptyTree_InsertionPointHasZeroBalanceFactor_MakePositive() {
//        LinkedAVLTree<Integer> testInsert = new LinkedAVLTree<>();
//        testInsert.insert(1);
//        testInsert.insert(10);
//        System.out.println(testInsert.getRoot().getBalanceFactor());
//        System.out.println(testInsert.getRoot().getRight().getBalanceFactor());
//        assertEquals(1, testInsert.getRoot().getBalanceFactor());
//        testInsert.insert(100);
//        assertEquals("10", testInsert.getRoot().toString());
//        assertEquals(0, testInsert.getRoot().getBalanceFactor());
//    }
//
//    @Test
//    public void testInsertIntoNonEmptyTree_InsertionPointHasZeroBalanceFactor_MakeNegative() {
//        LinkedAVLTree<Integer> testInsert = new LinkedAVLTree<>();
//        testInsert.insert(100);
//        testInsert.insert(10);
//        assertEquals(-1, testInsert.getRoot().getBalanceFactor());
//        testInsert.insert(1);
//        assertEquals("10", testInsert.getRoot().toString());
//        assertEquals(0, testInsert.getRoot().getBalanceFactor());
//    }
//
//    @Test
//    public void testInsertIntoNonEmptyTree_MultipleLevelsToRebalance() {
//        LinkedAVLTree<Integer> testInsert = new LinkedAVLTree<>();
//        testInsert.insert(10);
//        testInsert.insert(8);
//        testInsert.insert(12);
//        testInsert.insert(11);
//        testInsert.insert(13);
//        testInsert.insert(7);
//        testInsert.insert(9);
//        testInsert.insert(6);
//        System.out.println(testInsert.getRoot().getLeft().getLeft().getLeft());
//        System.out.println();
//        testInsert.insert(5);
//        System.out.println(testInsert.getRoot().getLeft().getLeft());
//        System.out.println(testInsert.getRoot().getLeft().getLeft().getLeft());
//        System.out.println(testInsert.getRoot().getLeft().getLeft().getRight());
//        assertEquals("6", testInsert.getRoot().getLeft().getLeft().toString());
//        System.out.println();
//        testInsert.insert(4);
//        System.out.println(testInsert.getRoot().getLeft());
//        System.out.println(testInsert.getRoot().getLeft().getLeft().getLeft());
//        assertEquals("6", testInsert.getRoot().getLeft().toString());
//        testInsert.insert(3);
//        testInsert.insert(2);
//        testInsert.insert(1);
//    }
//
//    @Test(expected = NoSuchElementException.class)
//    public void testDeleteFromEmptyTree() {
//        LinkedAVLTree<Integer> testDelete = new LinkedAVLTree<>();
//        testDelete.delete(1);
//    }
//
//    @Test
//    public void testDeleteFromNonEmptyTree_NoSuccessor_TargetIsLeftLeaf() {
//        LinkedAVLTree<Integer> testDelete = new LinkedAVLTree<>();
//        testDelete.insert(10);
//        testDelete.insert(1);
//        assertTrue(testDelete.getRoot().hasLeft());
//        testDelete.delete(1);
//        assertFalse(testDelete.getRoot().hasLeft());
//    }
//
//    @Test
//    public void testDeleteFromNonEmptyTree_NoSuccessor_TargetIsRightLeaf() {
//        LinkedAVLTree<Integer> testDelete = new LinkedAVLTree<>();
//        testDelete.insert(10);
//        testDelete.insert(100);
//        assertTrue(testDelete.getRoot().hasRight());
//        testDelete.delete(100);
//        assertFalse(testDelete.getRoot().hasRight());
//    }
//
//    @Test
//    public void testDeleteFromNonEmptyTree_NoSuccessor_TargetIsRoot() {
//        LinkedAVLTree<Integer> testDelete = new LinkedAVLTree<>(new LinkedAVLNode<>(10));
//        assertTrue(testDelete.hasRoot());
//        testDelete.delete(10);
//        assertFalse(testDelete.hasRoot());
//    }
}