package com.jonlandrum.collections.BinarySearchTree.AVLTree;

import com.jonlandrum.collections.BinarySearchTree.BinarySearchNode;

public interface AVLNode<T extends Comparable<T>> extends BinarySearchNode<T> {
    /**
     * Returns the balance factor of this node.
     *
     * @return The balance factor of this node
     */
    int getBalanceFactor();

    /**
     * Sets the balance factor of this node.
     */
    void setBalanceFactor();

    /**
     * Returns the height of this node.
     *
     * @return The height of this node
     */
    int getHeight();

    /**
     * Returns the height of the left subtree of this node.
     *
     * @return The height of the left subtree of this node
     */
    int getLeftHeight();

    int getRightHeight();

    /**
     * Sets the parent of this node to the node parameter.
     *
     * @param node The node to set as this node's parent
     */
    void setParent(AVLNode<T> node);

    /**
     * Sets the left child of this node to the node parameter.
     *
     * @param node The node to set as this node's left child
     */
    void setLeft(AVLNode<T> node);

    /**
     * Sets the right child of this node to the node parameter.
     *
     * @param node The node to set as this node's right child
     */
    void setRight(AVLNode<T> node);

    /**
     * Returns the node that replaces this node.
     *
     * <p>This method first attempts to locate the in-order successor of this node.
     * The in-order successor of a given node is the next element in the collection in sorted order.
     * If an in-order successor doesn't exist, this method returns the in-order predecessor.
     * If neither exists, this method returns an empty node.</p>
     *
     * @return The node that replaces this node
     */
    AVLNode<T> getReplacement();
}