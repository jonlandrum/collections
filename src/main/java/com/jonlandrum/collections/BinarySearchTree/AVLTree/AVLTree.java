package com.jonlandrum.collections.BinarySearchTree.AVLTree;

import com.jonlandrum.collections.BinarySearchTree.BinarySearchTree;

import java.util.NoSuchElementException;

public interface AVLTree<T extends Comparable<T>> extends BinarySearchTree<T> {
    /**
     * Returns the root node of this tree.
     *
     * @return The root node of this tree
     */
    AVLNode<T> getRoot();

    /**
     * Sets the root node of this tree to the node parameter.
     *
     * @param node The node to be set as this tree's root node
     */
//    void setRoot(AVLNode<T> node);

    /**
     * Inserts a new AVLNode into this tree.
     *
     * @param node The node to insert into this tree
     */
    AVLNode<T> insert(AVLNode<T> node);

    /**
     * Deletes a node from this tree.
     *
     * @param node The node to be removed from this tree
     * @return The replacement of the node removed from this tree
     */
    AVLNode<T> delete(AVLNode<T> node);

    /**
     * Searches for a specific node in this tree.
     *
     * @param node A node containing the same value as the node searched for
     * @return The node searched for
     * @throws NoSuchElementException if the desired node is not in this tree
     */
    AVLNode<T> search(AVLNode<T> node);
}