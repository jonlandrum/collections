package com.jonlandrum.collections.BinarySearchTree;

import java.util.NoSuchElementException;

public interface BinarySearchTree<T extends Comparable<T>> {
    /**
     * Returns the root node of this tree.
     *
     * @return The root node of this tree
     */
    BinarySearchNode<T> getRoot();

    /**
     * Sets the root node of this tree to the node parameter.
     *
     * @param node The node to be set as this tree's root node
     */
    public void setRoot(BinarySearchNode<T> node);

    /**
     * Sets the root node of this tree to null.
     */
    public void setRoot();

    /**
     * Inserts a new BinarySearchNode into this tree.
     *
     * @param node The node to insert into this tree
     */
    BinarySearchNode<T> insert(BinarySearchNode<T> node);

    /**
     * Deletes a node from this tree.
     *
     * @param node The node to be removed from this tree
     * @return The replacement of the node removed from this tree
     */
    BinarySearchNode<T> delete(BinarySearchNode<T> node);

    /**
     * Searches for a specific node in this tree.
     *
     * @param node A node containing the same value as the node searched for
     * @return The node searched for
     * @throws NoSuchElementException if the desired node is not in this tree
     */
    BinarySearchNode<T> search(BinarySearchNode<T> node);
}