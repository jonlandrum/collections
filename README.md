# `com.jonlandrum.collections`

[![Travis](https://img.shields.io/travis/jonlandrum/collections.svg)](https://travis-ci.org/jonlandrum/collections)
[![Codecov](https://img.shields.io/codecov/c/github/jonlandrum/collections.svg)](https://codecov.io/gh/jonlandrum/collections)
[![Maven Central](https://img.shields.io/maven-central/v/com.jonlandrum/collections.svg?label="maven central")](https://search.maven.org/#artifactdetails%7Ccom.jonlandrum%7Ccollections%7C1.0%7C)

This is a library of data structures, beginning with binary search trees and adjustable trees. New collections are constantly being added.

## Usage

This library can be included using Maven or by downloading the JAR manually. The project files are stored in the <a href="https://search.maven.org/#artifactdetails%7Ccom.jonlandrum%7Ccollections%7C1.0%7C" title="com.jonlandrum.collections on Maven Central Repository">Maven Central Repository</a>, so adding the library to your project is as simple as adding the dependency to your `pom.xml`:

```xml
<dependency>
    <groupId>com.jonlandrum</groupId>
    <artifactId>collections</artifactId>
    <version>1.0</version>
</dependency>
```

If you prefer to download the JAR manually, links to the JAR files are at the Maven Central Repository. After you have included the library, you simply need an `import` statement to start using the data structures:

```java
import com.jonlandrum.collections.*;
```

## Current Data Structures

At present, all of the data structures included in this library are types of Binary Search Trees:

* `com.jonlandrum.collections.BinarySearchTree`
* `com.jonlandrum.collections.BinarySearchTree.AVLTree`
* `com.jonlandrum.collections.BinarySearchTree.SplayTree`

Collections will be added to the library on a continuous basis.

## License and Copyright

<a rel="license" href="https://www.gnu.org/licenses/agpl.txt" title="GNU AGPL v3.0"><img alt="GNU LGPLv3 License" style="border-width:0" src="https://www.gnu.org/graphics/agplv3-88x31.png" /></a>

This library is Copyright &copy; 2017, <a href="http://jonlandrum.com/" title="jonlandrum.com">Jonathan E. Landrum</a>.

This library is free software: you can redistribute it and/or modify it under the terms of the <a rel="license" href="https://www.gnu.org/licenses/agpl.html" title="GNU AGPL v3.0">GNU Affero General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A copy of the GNU Affero General Public License is included with this library in the file named <a rel="license" href="LICENSE" title="GNU AGPL v3.0">LICENSE</a>.

In addition to the above licensing, a further restriction that redistribution **must include attribution to the original author** is employed.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" title="CC-BY-SA"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

The documentation contained in this library is Copyright &copy; 2017, <a href="http://jonlandrum.com/" title="jonlandrum.com">Jonathan E. Landrum</a>.

The documentation contained in this library is licensed under the <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" title="CC-BY-SA">Creative Commons Attribution-ShareAlike 4.0 International License</a>. This means that you are free to:

* *Share* &ndash; copy and redistribute the material in any medium or format
* *Adapt* &ndash; remix, transform, and build upon the material

for any purpose, even commercially.

The licensor cannot revoke these freedoms as long as you follow these terms:

* *Attribution* &ndash; You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* *ShareAlike* &ndash; If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

There are no additional restrictions. You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Notices:

* You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation
* No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material

A copy of the Creative Commons Attribution-ShareAlike 4.0 International License is included with this library in the file named <a rel="license" href="COPYING" title="CC-BY-SA">COPYING</a>.
