package com.jonlandrum.collections.BinarySearchTree.AVLTree;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedAVLNodeTest {
    @Test
    public void testEmptyConstructor() {
        LinkedAVLNode node = new LinkedAVLNode<>();
        assertEquals(null, node.getData());
    }

    @Test
    public void testDataConstructor() {
        LinkedAVLNode node = new LinkedAVLNode<>("test");
        assertEquals("test", node.getData());
    }

//    @Test
//    public void testGetBalanceFactor() {
//        LinkedAVLNode<String> node1 = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> node2 = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> node3 = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> node4 = new LinkedAVLNode<>("test");
//
//        assertEquals(0, node1.getBalanceFactor());
//
//        node1.setLeft(node2);
//        assertEquals(-1, node1.getBalanceFactor());
//
//        node1.setRight(node3);
//        assertEquals(0, node1.getBalanceFactor());
//
//        node3.setRight(node4);
//        assertEquals(1, node1.getBalanceFactor());
//    }
//
//    @Test
//    public void testGetData_DataIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>();
//        assertEquals(null, node.getData());
//    }
//
//    @Test
//    public void testGetData_DataIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertEquals("test", node.getData());
//    }
//
//    @Test
//    public void testGetParent_ParentIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertEquals(null, node.getParent());
//    }
//
//    @Test
//    public void testGetParent_ParentIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("test");
//        node.setParent(parent);
//        assertEquals(parent, node.getParent());
//    }
//
//    @Test
//    public void testGetLeft_LeftIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertEquals(null, node.getLeft());
//    }
//
//    @Test
//    public void testGetLeft_LeftIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> left = new LinkedAVLNode<>("test");
//        node.setLeft(left);
//        assertEquals(left, node.getLeft());
//    }
//
//    @Test
//    public void testGetRight_RightIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertEquals(null, node.getRight());
//    }
//
//    @Test
//    public void testGetRight_RightIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> right = new LinkedAVLNode<>("test");
//        node.setRight(right);
//        assertEquals(right, node.getRight());
//    }
//
//    @Test
//    public void testGetReplacement_TargetHasLeftChild() {
//        LinkedAVLNode<Integer> node1 = new LinkedAVLNode<>(1);
//        LinkedAVLNode<Integer> node2 = new LinkedAVLNode<>(2);
//        LinkedAVLNode<Integer> node3 = new LinkedAVLNode<>(3);
//        LinkedAVLNode<Integer> node4 = new LinkedAVLNode<>(4);
//        LinkedAVLNode<Integer> node5 = new LinkedAVLNode<>(5);
//        node5.setLeft(node1);
//        node1.setParent(node5);
//        node1.setRight(node2);
//        node2.setParent(node1);
//        node2.setRight(node3);
//        node3.setParent(node2);
//        node3.setRight(node4);
//        node4.setParent(node3);
//        assertTrue(node5.getReplacement().toString().equals("4"));
//    }
//
//    @Test
//    public void testGetReplacement_TargetHasRightChild() {
//        LinkedAVLNode<Integer> node1 = new LinkedAVLNode<>(1);
//        LinkedAVLNode<Integer> node2 = new LinkedAVLNode<>(2);
//        LinkedAVLNode<Integer> node3 = new LinkedAVLNode<>(3);
//        LinkedAVLNode<Integer> node4 = new LinkedAVLNode<>(4);
//        LinkedAVLNode<Integer> node5 = new LinkedAVLNode<>(5);
//        node1.setRight(node5);
//        node5.setParent(node1);
//        node5.setLeft(node4);
//        node4.setParent(node5);
//        node4.setLeft(node3);
//        node3.setParent(node4);
//        node3.setLeft(node2);
//        node2.setParent(node3);
//        assertTrue(node1.getReplacement().toString().equals("2"));
//    }
//
//    @Test
//    public void testGetReplacement_TargetHasNoChildren() {
//        LinkedAVLNode<Integer> node1 = new LinkedAVLNode<>(1);
//        LinkedAVLNode<Integer> node2 = new LinkedAVLNode<>(2);
//        LinkedAVLNode<Integer> node3 = new LinkedAVLNode<>(3);
//        LinkedAVLNode<Integer> node4 = new LinkedAVLNode<>(4);
//        LinkedAVLNode<Integer> node5 = new LinkedAVLNode<>(5);
//        node1.setRight(node5);
//        node5.setParent(node1);
//        node5.setLeft(node4);
//        node4.setParent(node5);
//        node4.setLeft(node3);
//        node3.setParent(node4);
//        node3.setLeft(node2);
//        node2.setParent(node3);
//        assertTrue(node2.getReplacement().toString().equals(""));
//    }
//
//    @Test
//    public void testSetData_DataIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>();
//        node.setData("test");
//        assertEquals("test", node.getData());
//    }
//
//    @Test
//    public void testSetData_DataIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        node.setData("success");
//        assertEquals("success", node.getData());
//    }
//
//    @Test
//    public void testSetParentAsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("test");
//        node.setParent(parent);
//        assertTrue(node.hasParent());
//        node.setParent();
//        assertFalse(node.hasParent());
//    }
//
//    @Test
//    public void testSetParentAsNode() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("test");
//        assertFalse(node.hasParent());
//        node.setParent(parent);
//        assertTrue(node.hasParent());
//    }
//
//    @Test
//    public void testSetLeftAsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> left = new LinkedAVLNode<>("test");
//        node.setLeft(left);
//        assertTrue(node.hasLeft());
//        node.setLeft();
//        assertFalse(node.hasLeft());
//    }
//
//    @Test
//    public void testSetLeftAsNode() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> left = new LinkedAVLNode<>("test");
//        assertFalse(node.hasLeft());
//        node.setLeft(left);
//        assertTrue(node.hasLeft());
//    }
//
//    @Test
//    public void testSetRightAsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> right = new LinkedAVLNode<>("test");
//        node.setRight(right);
//        assertTrue(node.hasRight());
//        node.setRight();
//        assertFalse(node.hasRight());
//    }
//
//    @Test
//    public void testSetRightAsNode() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        LinkedAVLNode<String> right = new LinkedAVLNode<>("test");
//        assertFalse(node.hasRight());
//        node.setRight(right);
//        assertTrue(node.hasRight());
//    }
//
//    @Test
//    public void testHasData_DataIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>();
//        assertFalse(node.hasData());
//    }
//
//    @Test
//    public void testHasData_DataIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertTrue(node.hasData());
//    }
//
//    @Test
//    public void testHasParent() {
//        LinkedAVLNode<Double> node1 = new LinkedAVLNode<>(1.1);
//        LinkedAVLNode<Double> node2 = new LinkedAVLNode<>(1.2);
//        assertFalse(node1.hasParent());
//        node1.setParent(node2);
//        assertTrue(node1.hasParent());
//    }
//
//    @Test
//    public void testHasLeft() {
//        LinkedAVLNode<Double> node1 = new LinkedAVLNode<>(1.1);
//        LinkedAVLNode<Double> node2 = new LinkedAVLNode<>(1.2);
//        assertFalse(node1.hasLeft());
//        node1.setLeft(node2);
//        assertTrue(node1.hasLeft());
//    }
//
//    @Test
//    public void testHasRight() {
//        LinkedAVLNode<Double> node1 = new LinkedAVLNode<>(1.1);
//        LinkedAVLNode<Double> node2 = new LinkedAVLNode<>(1.2);
//        assertFalse(node1.hasRight());
//        node1.setRight(node2);
//        assertTrue(node1.hasRight());
//    }
//
//    @Test
//    public void testHasChild_NoChildrenEverAssigned() {
//        LinkedAVLNode<Character> parent = new LinkedAVLNode<>('d');
//        assertFalse(parent.hasChild());
//    }
//
//    @Test
//    public void testHasChild_LeftChildAssigned() {
//        LinkedAVLNode<Character> parent = new LinkedAVLNode<>('d');
//        LinkedAVLNode<Character> child = new LinkedAVLNode<>('a');
//        parent.setLeft(child);
//        assertTrue(parent.hasChild());
//    }
//
//    @Test
//    public void testHasChild_RightChildAssigned() {
//        LinkedAVLNode<Character> parent = new LinkedAVLNode<>('d');
//        LinkedAVLNode<Character> child = new LinkedAVLNode<>('a');
//        parent.setRight(child);
//        assertTrue(parent.hasChild());
//    }
//
//    @Test
//    public void testHasChild_BothChildrenAssigned() {
//        LinkedAVLNode<Character> parent = new LinkedAVLNode<>('d');
//        LinkedAVLNode<Character> child1 = new LinkedAVLNode<>('a');
//        LinkedAVLNode<Character> child2 = new LinkedAVLNode<>('g');
//        parent.setLeft(child1);
//        parent.setRight(child2);
//        assertTrue(parent.hasChild());
//    }
//
//    @Test
//    public void testHasChild_ChildDeleted() {
//        LinkedAVLNode<Character> parent = new LinkedAVLNode<>('d');
//        LinkedAVLNode<Character> child = new LinkedAVLNode<>('a');
//        parent.setLeft(child);
//        parent.setLeft();
//        assertFalse(parent.hasChild());
//    }
//
//    @Test
//    public void testIsLeft_ParentIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("node");
//        assertFalse(node.isLeft());
//    }
//
//    @Test
//    public void testIsLeft_ParentIsNotNull_NodeIsLeftChild() {
//        LinkedAVLNode<String> child = new LinkedAVLNode<>("child");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("parent");
//        parent.setLeft(child);
//        child.setParent(parent);
//        assertTrue(child.isLeft());
//    }
//
//    @Test
//    public void testIsLeft_ParentIsNotNull_NodeIsRightChild() {
//        LinkedAVLNode<String> child = new LinkedAVLNode<>("child");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("parent");
//        parent.setRight(child);
//        child.setParent(parent);
//        assertFalse(child.isLeft());
//    }
//
//    @Test
//    public void testIsRight_ParentIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("node");
//        assertFalse(node.isRight());
//    }
//
//    @Test
//    public void testIsRight_ParentIsNotNull_NodeIsRightChild() {
//        LinkedAVLNode<String> child = new LinkedAVLNode<>("child");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("parent");
//        parent.setRight(child);
//        child.setParent(parent);
//        assertTrue(child.isRight());
//    }
//
//    @Test
//    public void testIsRight_ParentIsNotNull_NodeIsLeftChild() {
//        LinkedAVLNode<String> child = new LinkedAVLNode<>("child");
//        LinkedAVLNode<String> parent = new LinkedAVLNode<>("parent");
//        parent.setLeft(child);
//        child.setParent(parent);
//        assertFalse(child.isRight());
//    }
//
//    @Test
//    public void testToString_DataIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>();
//        assertEquals("", node.toString());
//    }
//
//    @Test
//    public void testToString_DataIsNotNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        assertEquals("test", node.toString());
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void testCompareTo_TargetIsNull() {
//        LinkedAVLNode<String> node = new LinkedAVLNode<>("test");
//        node.compareTo(new LinkedAVLNode<String>());
//    }
//
//    @Test
//    public void testCompareTo_TargetIsData() {
//        LinkedAVLNode<Integer> node = new LinkedAVLNode<>(1);
//        assertEquals(-1, node.compareTo(new LinkedAVLNode<>(2)));
//    }
//
//    @Test
//    public void testCompareTo_TargetIsNode() {
//        LinkedAVLNode<Integer> node = new LinkedAVLNode<>(1);
//        assertEquals(-1, node.compareTo(new LinkedAVLNode<>(2)));
//    }
}
