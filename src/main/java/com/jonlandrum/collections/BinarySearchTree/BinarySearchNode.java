package com.jonlandrum.collections.BinarySearchTree;

public interface BinarySearchNode<T extends Comparable<T>> {
    /**
     * Returns the data stored in this node.
     *
     * @return The data stored in this node
     */
    T getData();

    /**
     * Sets the data element of this node to the data parameter.
     *
     * @param data The data to store in this node
     */
    void setData(T data);

    /**
     * Returns the parent of this node.
     *
     * @return The parent of this node
     */
    BinarySearchNode<T> getParent();

    /**
     * Sets the parent of this node to the node parameter.
     *
     * @param node The node to set as this node's parent
     */
    void setParent(BinarySearchNode<T> node);

    /**
     * Returns the left child of this node.
     *
     * @return The left child of this node
     */
    BinarySearchNode<T> getLeft();

    /**
     * Returns the right child of this node.
     *
     * @return The right child of this node
     */
    BinarySearchNode<T> getRight();

    /**
     * Returns the node that replaces this node.
     *
     * <p>This method first attempts to locate the in-order successor of this node.
     * The in-order successor of a given node is the next element in the collection in sorted order.
     * If an in-order successor doesn't exist, this method returns the in-order predecessor.
     * If neither exists, this method returns an empty node.</p>
     *
     * @return The node that replaces this node
     */
    BinarySearchNode<T> getReplacement();

    /**
     * Sets the data element of this node to null.
     */
    void setData();

    /**
     * Sets the parent of this node to null.
     */
    void setParent();

    /**
     * Sets the left child of this node to null.
     */
    void setLeft();

    /**
     * Sets the right child of this node to null.
     */
    void setRight();

    /**
     * Returns true if the data element of this node is not null.
     *
     * @return True if the data element of this node is not null
     */
    boolean hasData();

    /**
     * Returns true if the parent of this node is not null.
     *
     * @return True if the parent of this node is not null
     */
    boolean hasParent();

    /**
     * Returns true if the left child of this node is not null.
     *
     * @return True if the left child of this node is not null
     */
    boolean hasLeft();

    /**
     * Returns true if the right child of this node is not null.
     *
     * @return True if the right child of this node is not null
     */
    boolean hasRight();

    /**
     * Returns true if either the left or right child of this node is not null.
     *
     * @return True if either the left or right child of this node is not null
     */
    boolean hasChild();

    /**
     * Returns true if this node is the left child of its parent.
     *
     * @return True if this node is the left child of its parent
     */
    boolean isLeft();

    /**
     * Sets the left child of this node to the node parameter.
     *
     * @param node The node to set as this node's left child
     */
    void setLeft(BinarySearchNode<T> node);

    /**
     * Returns true if this node is the right child of its parent.
     *
     * @return True if this node is the right child of its parent
     */
    boolean isRight();

    /**
     * Sets the right child of this node to the node parameter.
     *
     * @param node The node to set as this node's right child
     */
    void setRight(BinarySearchNode<T> node);

    /**
     * Returns a String containing the data element of this node.
     *
     * @return A String containing the data element of this node
     */
    @Override
    String toString();

    /**
     * Returns an integer representing the comparison between this node and that node.
     * <p>
     * <p>A negative value means this node is less than that node.
     * A positive value means this node is greater than that node.
     * A zero value means this node and that node are equal.</p>
     *
     * @param that A node against which this node should be compared
     * @return An integer representing the comparison between this node and that node
     */
    int compareTo(BinarySearchNode<T> that);
}