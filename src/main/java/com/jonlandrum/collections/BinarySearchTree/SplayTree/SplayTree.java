package com.jonlandrum.collections.BinarySearchTree.SplayTree;

import com.jonlandrum.collections.BinarySearchTree.BinarySearchNode;
import com.jonlandrum.collections.BinarySearchTree.BinarySearchTree;

public interface SplayTree<T extends Comparable<T>> extends BinarySearchTree<T> {
    /**
     * Inserts a new data element into this tree.
     *
     * @param data The data element to insert into this tree
     */
    BinarySearchNode<T> insert(T data);

    /**
     * Inserts a new node into this tree.
     *
     * @param node The node to insert into this tree
     */
    BinarySearchNode<T> insert(BinarySearchNode<T> node);

    /**
     * Deletes a data element from this tree.
     *
     * @param data The data element to be removed from this tree
     * @return The replacement of the data element removed from this tree
     */
    BinarySearchNode<T> delete(T data);

    /**
     * Deletes a node from this tree.
     *
     * @param node The node to be removed from this tree
     * @return The replacement of the node removed from this tree
     */
    BinarySearchNode<T> delete(BinarySearchNode<T> node);

    /**
     * Searches for a specific data element in this tree.
     *
     * @param data The data element of the same value as the node searched for
     * @return The node searched for
     */
    BinarySearchNode<T> search(T data);

    /**
     * Searches for a specific node in this tree.
     *
     * @param node A node containing the same value as the node searched for
     * @return The node searched for
     */
    BinarySearchNode<T> search(BinarySearchNode<T> node);
}